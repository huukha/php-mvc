<?php

class Model
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            try {
                self::$instance = new PDO('mysql:host=localhost;dbname=product', 'root', '');
                self::$instance->exec("SET NAMES 'utf8mb4'");
            } catch (PDOException $ex) {
                die($ex->getMessage());
            }
        }

        return self::$instance;
    }
}
