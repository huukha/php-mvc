<?php

require_once('Model.php');

class Game extends Model
{
    public function __construct()
    {
        $this->db = Model::getInstance();
    }

    public function getAllGames()
    {
        $stmt = $this->db->query('SELECT * FROM product WHERE quantity > 0 ORDER BY id DESC');
        $data = $stmt->fetchAll();
        
        return $data;
    }

    public function getAllGamesAdmin()
    {
        $stmt = $this->db->query('SELECT * FROM product ORDER BY id DESC');
        $data = $stmt->fetchAll();
        
        return $data;
    }

    public function getById($id)
    {
        $stmt = $this->db->prepare('SELECT * FROM product WHERE id = ? AND quantity > 0');
        $stmt->execute([$id]);

        $data = $stmt->fetch();
        
        return $data;
    }

    public function add($name, $image, $manufacturer, $description, $price, $quantity)
    {
        $stmt = $this->db->prepare('INSERT INTO product (name, image, manufacturer, description, price, quantity) VALUES (?, ?, ?, ?, ?, ?)');
        $stmt->execute([$name, $image, $manufacturer, $description, $price, $quantity]);

        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
