<?php

require_once('Model.php');

class Cart extends Model
{
    public function __construct()
    {
        $this->db = Model::getInstance();
    }

    public function getProductById($id)
    {
        $sql = "SELECT id, name, price FROM product WHERE id IN ($id) ORDER BY FIELD (id, $id)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $data = $stmt->fetchAll();
        
        return $data;
    }
}
