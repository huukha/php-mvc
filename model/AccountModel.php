<?php

require_once('Model.php');

class Account extends Model
{
    public function __construct()
    {
        $this->db = Model::getInstance();
    }

    public function getAccount($username, $pass)
    {
        $stmt = $this->db->prepare('SELECT * FROM account WHERE username = ? AND BINARY password = ?');
        $stmt->execute([$username, $pass]);

        $data = $stmt->fetch();
        
        return $data;
        
        // return $stmt->debugDumpParams();
    }
}
