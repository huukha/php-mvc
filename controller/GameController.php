<?php

require_once('Controller.php');
require_once('model/GameModel.php');

class GameController extends Controller
{
    public function __construct()
    {
        $this->game = new Game();
    }

    public function detail($id)
    {
        $data = $this->game->getById($id);

        if ($data) {
            return $this->view('detail/index', [
                'title' => $data['name'],
                'data' => $data,
            ]);
        } else {
            header('Location: ./');
        }
    }
}
