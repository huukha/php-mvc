<?php

require_once('Controller.php');
require_once('model/CartModel.php');
session_start();

class CartController extends Controller
{
    public function __construct()
    {
        $this->cart = new Cart();
    }

    public function addToCart()
    {
        $productId = $_POST['product-id'];

        // Check whether the product has been purchased or not
        if (isset($_SESSION['cart'][$productId])) {
            $quantity = $_SESSION['cart'][$productId]+1;
        } else {
            $quantity = 1;
        }

        // Save info product
        $_SESSION['cart'][$productId] = $quantity;

        $data['success'] = true;
        // $data['test'] = $_SESSION['cart'][$productId];

        header("Content-Type:application/json");
        echo json_encode($data);
    }


    public function getCart()
    {
        $flag = NULL;

        if (isset($_SESSION['cart'])) {
            foreach ($_SESSION['cart'] as $productId => $quantity) {
                if (isset($productId)) {
                    $flag = true;
                } else {
                    $false = false;
                }
            }

        } else {
            $flag =false;
        }



        if ($flag == false) {
            $data['success'] = false;
        } else {
            foreach ($_SESSION['cart'] as $productId => $quantity) {
                // generate to use in sql query
                $arrID[] = $productId;
                // generate to update quantity for each product
                $arrQuantity[] = $quantity;
            }

            $stringId = implode(",", $arrID);

            $result = $this->cart->getProductById($stringId);


            // $result = [];

            if (count($result) > 0) {
                for ($i=0; $i < count($result); $i++) {
                    $result[$i]['quantity'] = $arrQuantity[$i];

                    $data['success'] = true;
                    $data['cart'] = $result;
                }

            } else {
                $data['success'] = false;
            }
            
        }

        header("Content-Type:application/json");
        echo json_encode($data);
    }
}

?>