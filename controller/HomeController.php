<?php

require_once('Controller.php');
require_once('model/GameModel.php');

class HomeController extends Controller
{
    public function __construct()
    {
        $this->game = new Game();
    }

    public function index()
    {
        $data = $this->game->getAllGames();

        return $this->view('home/index', [
            'title' => 'OSC Game',
            'games' => $data,
        ]);
    }

    public function hi()
    {
        return $this->view('home/hi', [
            'title' => 'Test',
            'text' => 'hello world',
        ]);
    }
}
