<?php

class Controller
{
    public function view($file, $data = [])
    {
        if (file_exists('view/' . $file . '.php')) {
            extract($data);

            ob_start();
            require_once('view/' . $file . '.php');
            
            $content = ob_get_clean();

            require_once('view/layout/layout-fe.php');
        } else {
            die('View not found');
        }
    }
}