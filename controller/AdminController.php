<?php
require_once('Controller.php');
require_once('model/AccountModel.php');
require_once('model/GameModel.php');

session_start();

class AdminController extends Controller
{
    public function __construct()
    {
        $this->account = new Account();
        $this->game = new Game();
    }

    public function view($file, $data = [])
    {
        if (file_exists('view/' . $file . '.php')) {
            extract($data);

            ob_start();
            require_once('view/' . $file . '.php');
            
            $content = ob_get_clean();

            require_once('view/layout/layout-be.php');
        } else {
            die('View not found');
        }
    }

    public function login()
    {
    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	        $username = htmlspecialchars($_POST['username']);
	        $pass = htmlspecialchars($_POST['password']);

	        $result = $this->account->getAccount($username, $pass);
	        
	        $_SESSION['admin'] = true;

	        if ($result) {
	            $data['success'] = true;

	        } else {
	            $data['success'] = false;
	        }

	        header("Content-Type:application/json");
	        echo json_encode($data);
        // echo json_encode($result);
	    } else {
	    	header('Location: index.php?controller=AdminController');
	    }
    }

    public function logout() {
        unset($_SESSION['admin']);
        header('Location: index.php?controller=AdminController');
    }

    public function index()
    {
        $data = $this->game->getAllGamesAdmin();

        return $this->view('admin/index', [
            'title' => 'Danh sách game',
            'games' => $data,
        ]);
    }

    public function add()
    {
    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		$name = trim(htmlspecialchars($_POST['name']));
	        $manufacturer = trim(htmlspecialchars($_POST['manufacturer']));
	        $quantity = $_POST['quantity'];
	        $price = $_POST['price'];
	        // $image = $_POST['image'];
	        $description = trim(htmlspecialchars($_POST['description']));

	        // $data['test'] = $image;

	        $noError = true;

	        if ($name == '') {
	        	$noError = false;
	        	$data['message'] = 'Vui lòng nhập tên game';
	        } else if ($manufacturer == '') {
	        	$noError = false;
	        	$data['message'] = 'Vui lòng nhập tên nhà sản xuất';
	        } else if (!is_numeric($price)) {
	        	$noError = false;
	        	$data['message'] = 'Giá tiền phải là số';
	        } else if ($price < 0) {
	        	$noError = false;
	        	$data['message'] = 'Giá tiền phải lớn hơn hoặc bằng 0';
	        } else if (!is_numeric($quantity)) {
	        	$noError = false;
	        	$data['message'] = 'Số lượng phải là số';
	        } else if ($quantity < 0) {
	        	$noError = false;
	        	$data['message'] = 'Số lượng phải lớn hơn hoặc bằng 0';
	        } else if (!file_exists($_FILES['image']['tmp_name'])) {
	        	$noError = false;
	        	$data['message'] = 'Vui lòng chọn hình ảnh';
	        } elseif ($description == '') {
	        	$noError = false;
	        	$data['message'] = 'Vui lòng nhập chi tiết';
	        }

	        if (file_exists($_FILES['image']['tmp_name'])) {
	        	$image = basename($_FILES['image']['name']);
				$extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));
		        $targetDir = 'assets/image/';
		        $allowedExtension = array('jpg', 'jpeg', 'png');

		        $linkImage = $targetDir.date('Y-m-d_H-i-s_').$image;
		        if (!in_array($extension, $allowedExtension)) {
		        	$noError = false;
		        	$data['message'] = 'Chỉ cho phép ảnh định dạng .jpg .jpeg .png';
		        }
		        if ($_FILES['image']['size'] > 10000000) {
		        	$noError = false;
		        	$data['message'] = 'Kích cở ảnh tối đa 10MB';
		        }  
	        }

	        if ($noError) {
	        	$result = $this->game->add($name, $linkImage, $manufacturer, $description, $price, $quantity);

	        	if ($result) {
	        		move_uploaded_file($_FILES['image']['tmp_name'], $linkImage);
	        		$data['success'] = true;
	        	} else {
	        		$data['success'] = false;
	        		$data['message'] = 'Có lỗi xảy ra. Vui lòng thử lại';
	        	}
	        	
	        }
	        

	        header('Content-Type:application/json');
	        echo json_encode($data);
    	} else {
	        return $this->view('admin/add', [
	            'title' => 'Thêm game mới',
	            'text' => 'thêm',
	        ]);
	    }
    }

    public function import()
    {
    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		if(file_exists($_FILES['csv']['tmp_name'])){
    			$file = basename($_FILES['csv']['name']);
				$extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));

				if ($extension == 'csv') {
					$fileData = fopen($_FILES['csv']['tmp_name'], 'r');
					$totalRow = count(file($_FILES['csv']['tmp_name']));

					$rowIndex = 0;
					$countSuccess = 0;
					$errIndexArr = array();
					while($row = fgetcsv($fileData)) {
						$rowIndex++;

	                    $result = $this->game->add($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
	                    
	                    if ($result) {
	                    	$countSuccess++;
	                    } else {
	                    	array_push($errIndexArr, $rowIndex);
	                    }
	                }
					$data['total-row'] = $totalRow;
					$data['error-row'] = $errIndexArr;
					$data['total-success'] = $countSuccess;
					$data['success'] = true;
				} else {
					$data['success'] = false;
					$data['message'] = 'Chỉ hỗ trợ định dạng file .csv';
				}
				
    		} else {
    			$data['success'] = false;
    			$data['message'] = 'Vui lòng chọn file .csv';
    		}

	        header('Content-Type:application/json');
	        echo json_encode($data);

    	} else {    	
	        return $this->view('admin/import', [
	            'title' => 'Nhập danh sách game',
	            'text' => 'import',
	        ]);
	    }
    }
}
