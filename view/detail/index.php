<div class="px-3 py-2 mb-2 bg-white">
    <img src="<?= $data['image'] ?>" class="col-12 img-responsive my-3 px-0">

    <div class="d-flex justify-content-between">
        <div>
            <span class="text-danger"><?= $data['name'] ?></span>

            <div class="badge badge-danger text-white">
                <?= number_format($data['price'], 0, ',', '.') ?> <sup>đ</sup>
            </div>
            <p class="text-muted">Nhà phát hành: <?= $data['manufacturer']?></p>
        </div>
    </div>

    <button class="btn btn-success text-white" id="btn-add-to-cart" value="<?= $data['id']?>">Mua Ngay</button>

    <div class="my-3"><?= nl2br($data['description']) ?></div>
    
</div>