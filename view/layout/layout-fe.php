
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/css.css">
</head>
<body>
    <nav class="bg-danger">
        <div class="container-fluid">
            <div class="d-flex justify-content-between px-3 py-2">
                <a class="navbar-brand text-white" href="./">OSC Game</>

                <div>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#cart-modal"><i class="fal fa-shopping-cart fa-flip-horizontal"></i> Giỏ Hàng</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-md-3">
                <div class="bg-white rounded">
                    <div class="px-3 py-2 mb-3">
                        <a href="./">Danh sách game</a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>
    </div>


    <!-- Cart Modal -->
    <div class="modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Giỏ hàng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                        </thead>
                        <tbody id="product-list">
                            <tr class="text-center">
                                <td colspan="5">Chưa có sản phẩm nào</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Đóng</button>
                    <button type="button" class="btn btn-primary btn-sm">Đặt hàng</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/cart.js"></script>
</body>
</html>