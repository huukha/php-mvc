<?php
if (!isset($_SESSION['admin'])) {
    header('Location: view/admin/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>

    <!-- Stylesheet -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/css.css">
</head>
<body>
    <nav class="bg-danger">
        <div class="container-fluid">
            <div class="d-flex justify-content-between px-3 py-2">
                <a class="navbar-brand text-white" href="./">OSC Game</>

                <div>
                    <a href="#" class="btn btn-danger"><i class="fal fa-user"></i> Hi Admin!</a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#logout-modal"><i class="fal fa-sign-out-alt"></i> Đăng xuất</a>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-md-3 mb-3">
                <div class="bg-white rounded">
                    <div class="px-3 py-2">
                        <a href="index.php?controller=AdminController">Danh sách game</a>
                    </div>
                    <div class="px-3 py-2">
                        <a href="index.php?controller=AdminController&action=add">Thêm game mới</a>
                    </div>
                    <div class="px-3 py-2">
                        <a href="index.php?controller=AdminController&action=import">Nhập danh sách game</a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <?= $content ?>
            </div>
        </div>
    </div>


    <!-- Logout Modal -->
    <div class="modal fade" id="logout-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Đăng xuất</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>Bạn có chắc chắn đăng xuất</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Đóng</button>
                    <a href="index.php?controller=AdminController&action=logout" class="btn btn-danger btn-sm">Đăng xuất</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="assets/js/manage-game.js"></script>
</body>
</html>