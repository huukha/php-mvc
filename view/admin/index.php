<h4 class="rounded p-3 mb-3 bg-white">Danh sách game</h4>
<?php foreach ($games as $game): ?>
    <div class="rounded p-3 mb-3 bg-white">
        <div class="row">
            <img src="<?= $game['image'] ?>" class="col-3"/>
            <div class="col-9">
                <div class="mb-5">
                    <h6><?= $game['name'] ?> <span class="badge badge-danger text-white">
                        <?= number_format($game['price'], 0, ',', '.') ?><sup>đ</sup>
                    </span></h6>
                    <p>Nhà phát hành: <?= $game['manufacturer']?></p>
                    <p>Số lượng còn lại: <?= $game['quantity']?></p>
                </div>
                <!-- <a href="index.php?controller=GameController&action=detail&id=<?= $game['id'] ?>" class="btn btn-link btn-sm">Chi tiết</a> -->
            </div>
        </div>
    </div>
<?php endforeach ?>

