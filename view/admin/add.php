<h4 class="rounded p-3 mb-3 bg-white">Thêm game mới</h4>
<div class="rounded p-3 mb-3 bg-white">
	<form class="form-horizontal" id="add-game-form" action="" method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label class="control-label col-sm-3" for="name">Tên game:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="name" placeholder="Tên game" name="name" required="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="manufacturer">Nhà phát hành:</label>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="manufacturer" placeholder="Nhà phát hành" name="manufacturer" required="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="price">Giá:</label>
			<div class="col-sm-9">
				<input type="number" class="form-control" id="price" placeholder="Giá" min="1" name="price" required="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="quantity">Số lượng:</label>
			<div class="col-sm-9">
				<input type="number" class="form-control" id="quantity" placeholder="Số lượng" min="1" value="1" name="quantity" required="">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="image">Hình ảnh:</label>
			<div class="col-sm-9">
				<input type="file" class="form-control" id="image" accept=".jpg, .jpeg, .png" name="image" required="">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12">
				<img src="" height="200" id="image-preview">
			</div>
		</div>


		<div class="form-group">
			<label class="control-label col-sm-3" for="description">Chi tiết:</label>
			<div class="col-sm-9">
				<textarea class="form-control" placeholder="Mô tả chi tiết" name="description" required=""></textarea>
			</div>
		</div>

		<div class="form-group">        
		<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="btn btn-danger">Thêm</button>
		</div>
		</div>
	</form>
</div>