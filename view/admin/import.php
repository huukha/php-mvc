<h4 class="rounded p-3 mb-3 bg-white">Nhập danh sách game</h4>
<div class="rounded p-3 mb-3 bg-white">
	<form class="form-horizontal" id="import-game-form" action="" method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label class="control-label col-sm-3" for="file-csv">Vui lòng chọn file csv:</label>
			<div class="col-sm-9">
				<input type="file" class="form-control" id="file-csv" accept=".csv" name="csv" required="">
			</div>
		</div>

		<div class="form-group">        
		<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="btn btn-danger">Thêm</button>
		</div>
		</div>
	</form>
</div>