$(document).ready(function(){
	$('#add-game-form').submit(function(e){
		e.preventDefault();

		var gameData = new FormData($(this)[0]);

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: 'index.php?controller=AdminController&action=add',
			processData: false,
			contentType: false,
			data: gameData
		}).done(function(response){
			if (response.success) {
				window.location = 'index.php?controller=AdminController';
			} else {
				alert(response.message);
			}
		}).fail(function(jqXHR, statusText, errorThrown){
			console.log("Fail:"+ jqXHR.responseText);
			console.log(errorThrown);
		})
	})


	$('#image-preview').parent('div').parent('div').hide();
    function load_image(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-preview').attr('src',e.target.result).parent('div').parent('div').show();
            };
            reader.readAsDataURL(input. files[0]);
        }
    }

    $('#image').change(function () {
        load_image(this);
        if ($(this).val() == '') $('#image-preview').parent('div').parent('div').hide();
    });



    $('#import-game-form').submit(function(e){
    	e.preventDefault();

    	var gameData = new FormData($(this)[0]);

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: 'index.php?controller=AdminController&action=import',
			processData: false,
			contentType: false,
			data: gameData
		}).done(function(response){
			if (response.success) {
				alert('Nhập thành công ' + response['total-success'] + ' game trong tổng số ' + response['total-row'] + ' dòng.');
				if (response['error-row'].length > 0) {
					alert('Vui lòng kiểm tra lại dòng ' + response['error-row'].join(', ') + ' trong file csv.');
				}
				window.location = 'index.php?controller=AdminController';
			} else {
				alert(response.message);
			}
		}).fail(function(jqXHR, statusText, errorThrown){
			console.log("Fail:"+ jqXHR.responseText);
			console.log(errorThrown);
		})

    })
})