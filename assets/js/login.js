$('#login-form').submit(function(e){
	e.preventDefault();

	username = $('#username').val();
	password = $('#password').val();

	$.ajax({
		method: 'POST',
		url: '../../index.php?controller=AdminController&action=login',
		data: {username: username, password: password}
	}).done(function(response){
		if (response.success) {
			window.location = '../../index.php?controller=AdminController';
		} else {
			alert('Sai thông tin đăng nhập');
		}
	}).fail(function(jqXHR, statusText, errorThrown){
		console.log("Fail:"+ jqXHR.responseText);
		console.log(errorThrown);
	})
})