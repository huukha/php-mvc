getCart();

$('#btn-add-to-cart').click(function(){
	productID = $(this).val();
	$.ajax({
		method: 'POST',
		url: 'index.php?controller=CartController&action=addToCart',
		data: {'product-id': productID}
	}).done(function(response){
		getCart();
		$('#cart-modal').modal();
	}).fail(function(jqXHR, statusText, errorThrown){
		console.log("Fail:"+ jqXHR.responseText);
		console.log(errorThrown);
	})
})

function getCart(){
	$.ajax({
		method: 'GET',
		// dataType: 'html',
		// url: 'index.php?controller=CartController&action=addToCart',
		url: 'index.php?controller=CartController&action=getCart',
	}).done(function(response){
		if (response.success) {
			var rows = '';
			var total = 0;
			$.each(response.cart, function(i, product){
				subTotal = product.quantity * product.price;
				total+= subTotal;

				rows+= '<tr>';
				rows+= '<td>'+(i+1)+'</td>';
				rows+= '<td>'+product.name+'</td>';
				rows+= '<td>'+addDot(product.price)+'</td>';
				rows+= '<td>'+product.quantity+'</td>';
				rows+= '<td>'+addDot(subTotal)+'</td>';
				rows+= '</tr>';
			})

			rows+= '<tr><td colspan=5 class="text-right text-danger">Tổng: '+addDot(total)+' VND</td></tr>'
		} else {
			rows='<tr class="text-center"><td colspan="5">Chưa có sản phẩm nào</td></tr>'
		}

		$('#product-list').html(rows)
		// if(response.success) {
		// 	console.log('1');
		// } else {
		// 	console.log('0');
		// }
	}).fail(function(jqXHR, statusText, errorThrown){
		console.log("Fail:"+ jqXHR.responseText);
		console.log(errorThrown);
	})
}

function addDot(num) {
	return new Intl.NumberFormat().format(num)
}
