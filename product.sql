-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 19, 2020 at 06:39 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `manufacturer`, `description`, `price`, `quantity`) VALUES
(1, 'Fall Guys: Ultimate Knockout', 'https://steamcdn-a.akamaihd.net/steam/apps/1097150/header.jpg', 'Mediatonic', 'Bạn sẽ vào vai 1 hạt đậu rơi từ trên trời xuống để bắt đầu \"Shows\" Đó là cách Fall Guys bắt đầu trò chơi. Tối đa 60 hạt đậu với nhiều màu sắc khác nhau từ skin tự chọn. Tất cả sẽ tham gia vào từng màn game nhỏ để tìm ra người tồn tại sau cùng. 25 màn game khá đa dạng, chia thành 2 loại chơi đơn và phối hợp đồng đội. Chủ yếu là các cuộc đua vượt chướng ngại vật và một số game liên quan đến quả bóng tròn, đuổi bắt đuôi nhau. Game được lấy cảm hứng từ những gameshow thử thách trên truyền hình, vì vậy mục tiêu duy nhất của bạn sẽ chỉ là trở thành người về đích đầu tiên.Nhà phát triển Mediatonic đã nói rằng họ sẽ thêm vào các màn game mới trong tương lai. Việc trở thành cơn sốt của Fall Guys là một điều khá bất ngờ với giới chuyên môn\r\n\r\n', 150000, 8),
(2, 'PUBG', 'https://hgeqic7azi.vcdn.com.vn/image/cache/catalog/Anh-SP-New/PUBG-ratbt-457x213.png', 'Tencent Games', 'Sau một năm đầy biến động, cuối cùng PlayerUnknown’s Battlegrounds đã dần trở lại sân chơi khi cuối năm 2018 họ đã thu hút được số lượng đông đảo người chơi.\r\n\r\n\r\n\r\nĐối với các tựa game bắn súng khác, người chơi có thể dùng tiền để trang bị cho mình những vũ khí ưng ý nhất. Nhưng đối với game Pubg, tất cả người chơi đều có khởi đầu từ hai bàn tay trắng, nên cơ hội chiến thắng là tương đương nhau. Đây là điểm khác biệt đầu tiên tạo nên hứng thú cho người do tính cạnh tranh sinh tồn quyết liệt trong suốt trận đấu.\r\n\r\n\r\n \r\n\r\nMuốn chiến thắng trong trò chơi, đồng nghĩa với việc bạn phải là người chiến thắng cuối cùng trong trận đấu. Người chơi phải chiến đấu với 99 người cùng chơi khác trong cùng một trận đấu trên đảo hoang. Do có khởi điểm là như nhau nên người chơi phải tận dụng tất cả các kĩ năng chiến đấu, và chiến lược chơi phù hợp. Lưu ý là bạn luôn phải đề cao cảnh giác mọi lúc mọi nơi vì trong trận chiến sẽ không có bất kì đồng minh nào.\r\n\r\n\r\n \r\n\r\nTính năng person versus person (pvp) của game này cũng là điểm thu hút đối với người chơi. Với tính năng này, bạn luôn phải theo sát nhân vật của mình, không để cho nhân vật tự lực cánh sinh trong trận đấu. Người chơi luôn trong tư thế sẵn sàng chiến đấu và chấp nhận thua cuộc, vì đây là điều không thể tránh khỏi trong một trận đấu.', 250000, 10),
(3, 'Black Desert', 'https://steamcdn-a.akamaihd.net/steam/apps/836620/header.jpg', ' Pearl Abyss', 'Black Desert Online là một trò chơi nhập vai trực tuyến nhiều người chơi giả tưởng theo định hướng hộp cát được phát triển bởi công ty Hàn Quốc Pearl Abyss và được phát hành lần đầu cho Microsoft Windows vào năm 2015. Nó được biết đến với cái tên Black Desert Remastered từ năm 2018.\r\nBlack Desert Online là một tựa game MMORPG đúng nghĩa với hình ảnh tâm thanh sống động cộng với lối chơi đa dạng, nơi bạn có thể chiến đấu với quái vật và trải nghiệm game với nhiều tính năng thú vị như: câu cá, buôn bán, nấu ăn, giả kim thuật, huấn luyện ngựa,… và nhiều hơn thế nữa.\r\nSự sụp đổ của nền văn minh cổ đại gây ra bởi những viên đá đen huyền bí thường được tìm thấy trong sa mạc, mang trong mình nguồn sức mạnh to lớn khiến những người cổ đại chìm đắm trong sức mạnh dẫn đến sự suy tàn của một nền văn minh vĩ đại.\r\nBạn sẽ khám phá những bí mật ẩn giấu của nền văn minh cổ đại thông qua lịch sử của Calpheon và Valencia. Những ký ức đã mất và bí mật của Linh hồn đen sẽ được tiết lộ. Hành trình tìm kiếm bộ mặt thật của nền văn minh cổ đại quanh sa mạc Đen đang chờ đón bạn.', 40000, 10),
(4, 'Total War: THREE KINGDOMS', 'https://steamcdn-a.akamaihd.net/steam/apps/779340/header.jpg', 'Creative Assembly', 'Với một chặng đường dài đến 20 năm, dòng game Total War đã có những bước tiến với gần như những cải biến xuất hiện theo từng năm cả về cơ chế màn chơi, engine đồ họa lẫn các yếu tố thêm thắt để tạo ra các trò chơi luôn giữ được “lửa” trong lòng người hâm mộ của thể loại game chiến thuật.\r\nChính vì thế, có thể nói Total War: Three Kingdoms đã hội tụ tất cả tinh hoa của studio trong suốt thời gian qua với vô vàn các yếu tố quen thuộc và vừa vặn như “đo ni đóng giày” cho thể loại game pha trộn giữa 4X và chiến thuật nhóm (Squad Based Tactic) này.\r\nĐiều đó có nghĩa là bạn vẫn “tiến hành” trò chơi theo lối cũ, xây dựng và cân bằng các thành phần kinh tế – xã hội, nghiên cứu công nghệ, xây quân, sau đó mang lực lượng khổng lồ của mình “bem” khắp mọi nơi để mở rộng lãnh thổ của mình ra nhiều thành trì hơn nữa, xây dựng mối quan hệ với phe này, đánh bại phe khác và rồi dần đưa guồng máy chiến tranh rộng khắp để có thể giành được chiến thắng cuối cùng.\r\nĐơn giản là vậy, nên những người hâm mộ “gộc” của dòng game chỉ mất chừng 10 phút để có thể làm quen với giao diện được cải biên của dòng game cho phù hợp với bối cảnh trò chơi.\r\nThế nhưng khi đi càng sâu vào trong “chiến trận”, bạn sẽ dễ dàng phát hiện ra sự khác biệt không nhỏ của Total War: Three Kingdoms so với các phiên bản trước đây, thậm chí nhiều đến nỗi những công thức làm nên thành công của bạn trước đây cũng phải được đem ra cân nhắc lại.\r\nĐiểm khác biệt dễ thấy đầu tiên nằm ở chỗ Total War: Three Kingdoms đã tách rời các công trình sản xuất như nông trại, mỏ khai thác, điểm mậu dịch… ra khỏi thành, lũy và khu dân cư, tạo nên một tổng thể dày đặc các cứ điểm cần phải kiểm soát của người chơi trên bản đồ để tạo dựng cơ sở kinh tế, chính trị cho một vùng đất.', 499000, 10),
(10, 'Hand Simulator: Survival', 'https://steamcdn-a.akamaihd.net/steam/apps/924140/header.jpg', 'HFM Games', 'Hand Simulator mang đến cho game thủ cảm giác thực sự \\\"thực tế\\\" khi tương tác với nhiều môi trường thực tế khác nhau. Có nhiều kịch bản khác nhau sẽ gặp phải. Một số trong số này đơn giản như chơi một trò chơi cờ vua hoặc câu cá. Những người khác như điều khiển vũ khí đòi hỏi rất nhiều kỹ năng và do đó, một đường cong học tập đáng kể là tuyệt vời cho những người đang tìm kiếm một thử thách xúc giác. Nền tảng này đang được phát triển ổn định, vì vậy nhiều cấp độ đang được thêm vào. Các cấp độ này thường là sáng tạo trực tiếp của chính người chơi.\\r\\nHand Simulator cung cấp chế độ nhiều người chơi, vì vậy bạn bè có thể tham gia vào cuộc vui. Thành tích được trao trên đường đi và chúng được đăng trong cộng đồng ảo. Ngoài ra còn có một chế độ ngoại tuyến có thể được chơi.', 30000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
