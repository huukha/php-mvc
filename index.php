<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$id = isset($_GET['id']) ? intval($_GET['id']) : null;

$controller = isset($_GET['controller']) ? htmlspecialchars($_GET['controller']) : 'HomeController';
$action = isset($_GET['action']) ? htmlspecialchars($_GET['action']) : 'index';

if (file_exists('controller/' . $controller . '.php')) {
    require_once('controller/' . $controller . '.php');
} else {
    die('Controller does\'nt exists');
}

$initController = new $controller();

if (method_exists($controller, $action)) {
    $initController->$action($id);
} else {
    die('Method not found!!!');
}
